<?php
require 'animal.php';
require 'Ape.php';
require 'Frog.php';

$sheep = new animal("shaun");

echo "Name : " . $sheep->name; // "shaun"
echo "<br>Legs : " . $sheep->legs; // 4
echo "<br>Cold Blooded : " . $sheep->cold_blooded; // "no"

$kodok = new Frog("buduk");
echo "<br><br>Name : " . $kodok->name;
echo "<br>Legs : " . $kodok->legs;
echo "<br>Cold Blooded : " . $kodok->cold_blooded;
echo "<br>Jump : " . $kodok->jump();  // "hop hop

$sungokong = new Ape("kera sakti");
echo "<br><br>Name : " . $sungokong->name;
echo "<br>Legs : " . $sungokong->legs;
echo "<br>Cold Blooded : " . $sungokong->cold_blooded;
echo "<br>Yell : " . $sungokong->yell(); // "Auooo"



// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())