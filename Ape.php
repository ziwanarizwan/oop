<?php

class Ape
{
    public $name = "Kera Sakti";
    public  $legs = 2;
    public $cold_blooded = "no";

    public function __construct($string)
    {
        $this->name = $string;
    }

    public function yell()
    {
        return "Auooo";
    }
}
