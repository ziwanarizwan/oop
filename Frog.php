<?php

class Frog
{
    public $name = "Buduk";
    public  $legs = 4;
    public $cold_blooded = "no";

    public function __construct($string)
    {
        $this->name = $string;
    }

    public function jump()
    {
        return "hop hop";
    }
}
